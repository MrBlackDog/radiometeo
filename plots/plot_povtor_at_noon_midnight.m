function [out] = plot_povtor_at_noon_midnight(in,heigth,plot)

[in_noon] = get_meas_at_noon(in);
[in_midnigth] = get_meas_at_midnigth(in);
%% Полночь
%% находим общее число измерений меньше 100 метров
[in_midnigth_data_less_h] = find_heigth_less_heigth(in_midnigth,heigth);
[in_midnigth_data_less_h_month] = get_meas_by_month(in_midnigth_data_less_h);
%% находим число инверсий меньше 100 метров
[~,in_midnigth_inv_data] = find_temp_iversion(in_midnigth);
[in_midnigth_inv_data_less_h] = find_heigth_less_heigth(in_midnigth_inv_data,heigth);
[in_midnigth_inv_data_less_h_month] = get_meas_by_month(in_midnigth_inv_data_less_h);
%% Полдень
%% находим общее число измерений меньше 100 метров
[in_noon_data_less_h] = find_heigth_less_heigth(in_noon,heigth);
[in_noon_data_less_h_month] = get_meas_by_month(in_noon_data_less_h);
%% находим число инверсий меньше 100 метров
[~,in_noon_inv_data] = find_temp_iversion(in_noon);
[in_noon_inv_data_less_h] = find_heigth_less_heigth(in_noon_inv_data,heigth);
[in_noon_inv_data_less_h_month] = get_meas_by_month(in_noon_inv_data_less_h);
%% Выходные данные
out.midnigth_inv_less_h_month = in_midnigth_inv_data_less_h_month;
out.midnigth_data_less_h_month = in_midnigth_data_less_h_month;
out.noon_inv_less_h_month = in_noon_inv_data_less_h_month;
out.noon_data_less_h_month = in_noon_data_less_h_month;
%% строим график повторяемости
if(plot)
    plot_povtor(in_midnigth_inv_data_less_h_month,in_midnigth_data_less_h_month,'r -*');
    plot_povtor(in_noon_inv_data_less_h_month,in_noon_data_less_h_month,'b -x');
end
end

