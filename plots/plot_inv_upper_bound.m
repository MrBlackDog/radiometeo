function [max_h_noon,max_h_midnigth] = plot_inv_upper_bound(in,heigth,plot)

[in_noon] = get_meas_at_noon(in);
[in_midnigth] = get_meas_at_midnigth(in);
%% находим число инверсий меньше heigth метров
[~,in_midnigth_inv_data] = find_temp_iversion(in_midnigth);
[in_midnigth_inv_data_less_heigth] = find_heigth_less_heigth(in_midnigth_inv_data,heigth);
[in_midnigth_inv_data_less_heigth_month] = get_meas_by_month(in_midnigth_inv_data_less_heigth);
%% Полдень
%% находим число инверсий меньше heigth метров
[~,in_noon_inv_data] = find_temp_iversion(in_noon);
[in_noon_inv_data_less_heigth] = find_heigth_less_heigth(in_noon_inv_data,heigth);
[in_noon_inv_data_less_heigth_month] = get_meas_by_month(in_noon_inv_data_less_heigth);

% [~,in_inv_data] = find_temp_iversion(in);
% [in_inv_data_less_heigth] = find_heigth_less_heigth(in_inv_data,heigth);
% [in_inv_data_less_heigth_month] = get_meas_by_month(in_inv_data_less_heigth);
[max_h_midnigth] = get_max_prizem_h(in_midnigth_inv_data_less_heigth_month);
[max_h_noon] = get_max_prizem_h(in_noon_inv_data_less_heigth_month);
month = 1:1:12;
if(plot)
    hold on
    plot(month,max_h_midnigth,'b -^')
    plot(month,max_h_noon,'r -o')
end
end