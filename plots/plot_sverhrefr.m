function [out] = plot_sverhrefr(in,plot)

[in_not_nan] = find_non_nan_relative_humidity(in);
for i = 1:length(in_not_nan)
    for j = 1: length(in_not_nan(i).data)
    grad(i).date = in_not_nan(i).date;
    grad(i).data(j).n = calc_n(in_not_nan(i).data(j));
    grad(i).data(j).h = in_not_nan(i).data(j).geopotential_height;
    end
    TFh = ~isnan([grad(i).data(:).h]);
    TFn = ~isnan([grad(i).data(:).n]);
    TF = and(TFh,TFn);
    ind = find(TF);
    if(length(ind)>1 && grad(i).data(ind(2)).h<1000)
        grad(i).dndh = (grad(i).data(ind(2)).n -  grad(i).data(ind(1)).n)/( grad(i).data(ind(2)).h - grad(i).data(ind(1)).h);
    else
        grad(i).dndh = NaN;
    end
end
ind_non_nan = ~isnan(extractfield(grad,'dndh'));
grad_non_nan = grad(ind_non_nan);
k=0;
for i =1: length(grad_non_nan)
    if(grad_non_nan(i).dndh <=-15.696*1e-5)
        k=k+1;
        sverh(k) = grad_non_nan(i);
    end
end
[out.in_month] = get_meas_by_month(in);
[out.sverh_month] = get_meas_by_month(sverh);

if(plot)
    plot_povtor(out.sverh_month,out.in_month,'r -x')
end
end

