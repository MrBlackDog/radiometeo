function [in_heigth] = plot_data_prizemnoe(in,heigth,data_pointer,plot)

[in_heigth] = get_meas_number_prizemnoe(in,heigth);
if(plot)
    hold on
    plot(extractfield(in_heigth,'year'), extractfield(in_heigth,'count'),data_pointer);
end
end

