function [] = plot_povtor(inv_data,all_data,data_pointer)

month = 1:1:12;
mass = [length(inv_data.month1)/length(all_data.month1)
        length(inv_data.month2)/length(all_data.month2)
        length(inv_data.month3)/length(all_data.month3)
        length(inv_data.month4)/length(all_data.month4)
        length(inv_data.month5)/length(all_data.month5)
        length(inv_data.month6)/length(all_data.month6)
        length(inv_data.month7)/length(all_data.month7)
        length(inv_data.month8)/length(all_data.month8)
        length(inv_data.month9)/length(all_data.month9)
        length(inv_data.month10)/length(all_data.month10)
        length(inv_data.month11)/length(all_data.month11)
        length(inv_data.month12)/length(all_data.month12)];
hold on
plot(month,mass,data_pointer)
end

