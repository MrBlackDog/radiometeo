function [in_1000_hPa] = plot_data_at_1000hPa(in,plot,data_pointer)

[in_1000_hPa] = get_meas_number_at_1000hPa(in);
plot(extractfield(in_1000_hPa,'year'), extractfield(in_1000_hPa,'count'),data_pointer);

end

