function [out] = get_inv_in_heigth(in,heigth)
%% находим общее число измерений меньше заданного значения heigth
[data_less_heigth] = find_heigth_less_heigth(in,heigth);
[data_less_heigth_month] = get_meas_by_month(data_less_heigth);
%% находим число инверсий меньше заданного значения heigth
[~,inv_data] = find_temp_iversion(in);
[inv_data_less_heigth] = find_heigth_less_heigth(inv_data,heigth);
[inv_data_less_heigth_month] = get_meas_by_month(inv_data_less_heigth);
%% выходные данные 
out.inv_data_less_heigth_month = inv_data_less_heigth_month;
out.data_less_heigth_month = data_less_heigth_month;
end

