function [max_h] = get_max_prizem_h(in)
h1 = zeros(1,length(in.month1));
h2 = zeros(1,length(in.month2));
h3 = zeros(1,length(in.month3));
h4 = zeros(1,length(in.month4));
h5 = zeros(1,length(in.month5));
h6 = zeros(1,length(in.month6));
h7 = zeros(1,length(in.month7));
h8 = zeros(1,length(in.month8));
h9 = zeros(1,length(in.month9));
h10 = zeros(1,length(in.month10));
h11 = zeros(1,length(in.month11));
h12 = zeros(1,length(in.month12));
for i=1:length(in.month1)
    h1(i) = in.month1(i).data(2).geopotential_height;
end
for i=1:length(in.month2)
    h2(i) = in.month2(i).data(2).geopotential_height;
end
for i=1:length(in.month3)
    h3(i) = in.month3(i).data(2).geopotential_height;
end
for i=1:length(in.month4)
    h4(i) = in.month4(i).data(2).geopotential_height;
end
for i=1:length(in.month5)
    h5(i) = in.month5(i).data(2).geopotential_height;
end
for i=1:length(in.month6)
    h6(i) = in.month6(i).data(2).geopotential_height;
end
for i=1:length(in.month7)
    h7(i) = in.month7(i).data(2).geopotential_height;
end
for i=1:length(in.month8)
    h8(i) = in.month8(i).data(2).geopotential_height;
end
for i=1:length(in.month9)
    h9(i) = in.month9(i).data(2).geopotential_height;
end
for i=1:length(in.month10)
    h10(i) = in.month10(i).data(2).geopotential_height;
end
for i=1:length(in.month11)
    h11(i) = in.month11(i).data(2).geopotential_height;
end
for i=1:length(in.month12)
    h12(i) = in.month12(i).data(2).geopotential_height;
end
max_h = [max(h1) max(h2) max(h3) max(h4) max(h5) max(h6) max(h7) max(h8) max(h9) max(h10) max(h11) max(h12)];

end

