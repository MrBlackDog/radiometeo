function [out] = get_meas_number_prizemnoe(in,heigth)

k = 0;
for i=1:length(in)
    for j=1:length(in(i).data)
        if(in(i).data(j).geopotential_height <= heigth)
            k = k + 1;
            year(k) = in(i).date.year;
        end
    end
end
%%
k=0;
i_prev = 0;
for i=2:length(year)
    if(year(i)-year(i-1)==1)
        k=k+1;
        out(k).year = year(i-1);
        out(k).count = i-i_prev;    
        i_prev = i;
    end
end

end

