function [in_prizemnoe,in_1000_hPa,in_inv_h,in_povtor_hour,hour,in_sverh] = get_data_for_all_figs(in,heigth)

in_prizemnoe = get_meas_number_prizemnoe(in,heigth);
in_1000_hPa = get_meas_number_at_1000hPa(in);
in_inv_h = get_inv_in_heigth(in,heigth);
try
    in_povtor_hour = plot_povtor_at_noon_midnight(in,heigth,0);
catch
    in_povtor_hour = NaN;
end
try
    [hour.noon_up_month,hour.midnigth_up_month] = plot_inv_upper_bound(in,heigth,0);
catch
    hour = NaN;
end
try
    [in_sverh] = plot_sverhrefr(in,0);
catch
    in_sverh = NaN;
end
end

