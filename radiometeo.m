function radiometeo
% Такие секции запускаются клавишами ctrl+enter 
% или через кнопку Run Section во вкладке Run панели инструментов
% это нужно, чтобы не запускать разом весь код, а делать его по частям
heigth = 1000;

%% Читает лог, но медленней
% [out] = RMS_file_reader();
%% Читает лог, чуть быстрее,чем медленно
%% Туапсе
[tuapse_data] = RMS_file_reader_parallel('RSM00037011-data.txt');% Туапсе
%% туапсе до 1973 года
%% Получает все данные для Туапсе, необходимые для отрисовки
[tuapse_prizemnoe,tuapse_1000_hPa,tuapse_inv_h,tuapse_povtor_hour,tuapse_upper_bound_hour,tuapse_sverh_month] = get_data_for_all_figs(tuapse_data,heigth);
%% Ростов на дону
[rostov_na_dony_data] = RMS_file_reader_parallel('RSM00034731-data.txt');
%% Получает все данные для Ростова, необходимые для отрисовки
[rostov_prizemnoe,rostov_1000_hPa,rostov_inv_h,rostov_povtor_hour,rostov_upper_bound_hour,rostov_sverh_month] = get_data_for_all_figs(rostov_na_dony_data,heigth);
%% Воронеж
[voronesh_data] = RMS_file_reader_parallel('RSM00034122-data.txt');
%% Получает все данные для Воронежа, необходимые для отрисовки
[voronesh_prizemnoe,voronesh_1000_hPa,voronesh_inv_h,voronesh_povtor_hour,voronesh_upper_bound_hour,voronesh_sverh_month] = get_data_for_all_figs(voronesh_data,heigth);
%% Одесса
[odessa_data] = RMS_file_reader_parallel('UPM00033837-data.txt');
%% Получает все данные для Одессы, необходимые для отрисовки
[odessa_prizemnoe,odessa_1000_hPa,odessa_inv_h,odessa_povtor_hour,odessa_upper_bound_hour,odessa_sverh_month] = get_data_for_all_figs(odessa_data,heigth);
%% Симферополь
[simpheropol_data] = RMS_file_reader_parallel('UPM00033946-data.txt');
%% Получает все данные для Симферополя, необходимые для отрисовки
[simpheropol_prizemnoe,simpheropol_1000_hPa,simpheropol_inv_h,simpheropol_povtor_hour,simpheropol_upper_bound_hour,simpheropol_sverh_month] = get_data_for_all_figs(simpheropol_data,heigth);
%% Читает лог
% [kluchi_data] = RMS_file_reader_parallel('RSM00032389-data.txt');
%% Обрезка лога по дате
% [kluchi] = find_obs_by_data(kluchi_data,year_begin,year_end);
% clear('kluchi_data')
%% Получает данне, необходимые для постройки рисунков
% [kluchi_prizemnoe,kluchi_1000_hPa,kluchi_inv_h,kluchi_povtor_hour,kluchi_upper_bound_hour,kluchi_sverh_month] = get_data_for_all_figs(kluchi_data,heigth);
%% Код для обрезки по дате, даты начала и конца ниже
year_begin = 1950;% начало
year_end = 1972;%конец
%% Туапсе 
[tuapse] = find_obs_by_data(tuapse_data,year_begin,year_end);
clear('tuapse_data')
%% Ростов на дону
[rostov_na_dony] = find_obs_by_data(rostov_na_dony_data,year_begin,year_end);
clear('rostov_na_dony_data')
%% Воронеж
[voronesh] = find_obs_by_data(voronesh_data,year_begin,year_end);
clear('voronesh_data')
%% Одесса
[odessa] = find_obs_by_data(odessa_data,year_begin,year_end);
clear('odessa_data')
%% Симферополь
[simpheropol] = find_obs_by_data(simpheropol_data,year_begin,year_end);
clear('simpheropol_data')

%% fig 1
figure
hold on
%plot(extractfield(kluchi_prizemnoe,'year'),%extractfield(kluchi_prizemnoe,'count'),data_pointer); 
%строка выше для примера, сделать аналогично, но для своих данных

%если этот график получается фиговым,то надо менять высоту и перезапускать
%код 
plot(extractfield(tuapse_prizemnoe,'year'),extractfield(tuapse_prizemnoe,'count'),'b -x');
plot(extractfield(rostov_prizemnoe,'year'),extractfield(rostov_prizemnoe,'count'),'r -o');
plot(extractfield(voronesh_prizemnoe,'year'),extractfield(voronesh_prizemnoe,'count'),'k -d');
plot(extractfield(odessa_prizemnoe,'year'),extractfield(odessa_prizemnoe,'count'),'m -^');
plot(extractfield(simpheropol_prizemnoe,'year'),extractfield(simpheropol_prizemnoe,'count'),'c -+');
legend('Туапсе','Ростов-на-Дону','Воронеж','Одесса','Симферополь')
grid on
axis([1980 2021 0 2200])
xtickangle(90)
xticks(1980:2:2021)
yticks(0:200:2200)
xlabel('годы')
ylabel('число измерений')
%% fig 2
figure
hold on
%plot(extractfield(kluchi_1000_hPa,'year'), extractfield(kluchi_1000_hPa,'count'),'b -d');
%строка выше для примера
plot(extractfield(tuapse_1000_hPa,'year'), extractfield(tuapse_1000_hPa,'count'),'b -x');
plot(extractfield(rostov_1000_hPa,'year'), extractfield(rostov_1000_hPa,'count'),'r -o');
plot(extractfield(voronesh_1000_hPa,'year'), extractfield(voronesh_1000_hPa,'count'),'k -d');
plot(extractfield(odessa_1000_hPa,'year'), extractfield(odessa_1000_hPa,'count'),'m -^');
plot(extractfield(simpheropol_1000_hPa,'year'), extractfield(simpheropol_1000_hPa,'count'),'c -+');
legend('Туапсе','Ростов-на-Дону','Воронеж','Одесса','Симферополь')
grid on
% axis([1980 2021 0 2200])
xtickangle(90)
xticks(1980:2:2021)
% yticks(0:200:2200)
xlabel('годы')
ylabel('число измерений')
%% fig 3 w/o graphs
%% fig 4
figure
hold on
%plot_povtor(kluchi_inv_h.inv_data_less_heigth_month,kluchi_inv_h.data_less_heigth_month,'b -*');
%строка выше для примера
plot_povtor(tuapse_inv_h.inv_data_less_heigth_month,tuapse_inv_h.data_less_heigth_month,'b -x');
plot_povtor(rostov_inv_h.inv_data_less_heigth_month,rostov_inv_h.data_less_heigth_month,'r -o');
plot_povtor(voronesh_inv_h.inv_data_less_heigth_month,voronesh_inv_h.data_less_heigth_month,'k -d');
plot_povtor(odessa_inv_h.inv_data_less_heigth_month,odessa_inv_h.data_less_heigth_month,'m -^');
plot_povtor(simpheropol_inv_h.inv_data_less_heigth_month,simpheropol_inv_h.data_less_heigth_month,'c -+');
legend('Туапсе','Ростов-на-Дону','Воронеж','Одесса','Симферополь')
grid on
axis([0 12.5 0 0.6])
xticks(1:1:12)
xticklabels({'I','II','III','IV','V','VI','VII','IX','X','XI','XII'})
yticklabels({'0','10','20','30','40','50','60'})
xlabel('номер месяца')
ylabel('повторяемость,%')
%% fig 5
figure
hold on
plot_povtor(tuapse_povtor_hour.midnigth_inv_less_h_month,tuapse_povtor_hour.midnigth_data_less_h_month,'r -*');
plot_povtor(tuapse_povtor_hour.noon_inv_less_h_month,tuapse_povtor_hour.noon_data_less_h_month,'b -x');
grid on
axis([0 12.5 0 1])
xticks(1:1:12)
xticklabels({'I','II','III','IV','V','VI','VII','IX','X','XI','XII'})
yticklabels({'0','10','20','30','40','50','60','70','80','90','100'})
xlabel('месяц')
ylabel('повторяемость,%')
legend('полночь','полдень')
%строки выше для примера
%% fig 6
figure
hold on
month = 1:1:12;
hold on
% plot(month,kluchi_upper_bound_hour.max_h_midnigth,'b -^')
% plot(month,kluchi_upper_bound_hour.max_h_noon,'r -o')
%строки выше для примера
%% fig 7
figure()
hold on
% plot_povtor(kluchi_sverh_month.sverh_month,kluchi_sverh_month.in_month,'r -x')
%строка выше для примера

plot_povtor(tuapse_sverh_month.sverh_month,tuapse_sverh_month.in_month,'b -x')
plot_povtor(rostov_sverh_month.sverh_month,rostov_sverh_month.in_month,'r -o')
plot_povtor(voronesh_sverh_month.sverh_month,voronesh_sverh_month.in_month,'k -d')
plot_povtor(odessa_sverh_month.sverh_month,odessa_sverh_month.in_month,'m -^')
plot_povtor(simpheropol_sverh_month.sverh_month,simpheropol_sverh_month.in_month,'c -+')
legend('Туапсе','Ростов-на-Дону','Воронеж','Одесса','Симферополь')