function [out] = find_non_nan_relative_humidity(in)

k=0;
for i=1:length(in)
    for j=1:length(in(i).data)
        if(~isnan(in(i).data(j).relative_humidity) && (~isnan(in(i).data(j).temperature)))
            k=k+1;
            out(k) = in(i);
            break;
        end
    end
end


end