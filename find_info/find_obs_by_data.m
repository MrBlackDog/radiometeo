function [out] = find_obs_by_data(in,year_begin,year_end)

k = 0;
% year_begin = 2000;
% year_end = 2021;
for i=1:length(in)
    if(in(i).date.year >= year_begin && in(i).date.year <= year_end)
        k = k + 1;
        out(k) = in(i);
    end
end

end

