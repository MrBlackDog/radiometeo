function [inv,inv_data] = find_temp_iversion(in)

k=0;
for i=1:length(in)
    for j = 2:length(in(i).data)
        if(~isnan(in(i).data(j).temperature))
            if(in(i).data(j).temperature>=in(i).data(j-1).temperature)
                k=k+1;
                inv(k) = in(i);
                inv_data(k).date = in(i).date;
                inv_data(k).data_count = in(i).data_count;
                inv_data(k).data_sourse = in(i).data_sourse;
                inv_data(k).latitude = in(i).latitude;
                inv_data(k).longitude = in(i).longitude;
                inv_data(k).data(1) = in(i).data(j-1);
                inv_data(k).data(2) = in(i).data(j);
%                 inv_data(k).temp = [in(i).data(j-1).temperature in(i).data(j).temperature];
%                 inv_data(k).pres = [in(i).data(j-1).pressure in(i).data(j).pressure];
%                 inv_data(k).heigth = [in(i).data(j-1).geopotential_height in(i).data(j).geopotential_height];
                break;
            end
        end
    end
end
end

