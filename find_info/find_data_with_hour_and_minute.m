function [out] = find_data_with_hour_and_minute(in)

k = 0;
year_begin = 1980;
for i=1:length(in)
    if(~isnan(in(i).date.hour) && in(i).date.year >= year_begin)
        k = k + 1;
        out(k) = in(i);
    end
end

end

