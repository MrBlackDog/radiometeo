function [saturation_vapour_pressure] = calculate_saturation_vapour_pressure(temperature,pressure,type)

%type is Water or Ice
switch type
    case 'Water'
        a = 6.1121;
        b = 18.678;
        c = 257.14;
        d = 234.5;
        EF = 1 + 1e-4 * (7.2 + pressure*(0.00320+5.9*1e-7*temperature^2));
    case 'Ice'
        a = 6.1115;
        b = 23.036;
        c = 279.82;
        d = 333.7;
        EF = 1 + 1e-4 * (2.2 + pressure*(0.00322+6.4*1e-7*temperature^2));
end
saturation_vapour_pressure = EF*a*exp( ((b-temperature/d)*temperature)/(temperature+c));        
end

