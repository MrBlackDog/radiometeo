function [N] =  calculate_N(in)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[e_s] = calculate_saturation_vapour_pressure(in.temperature,in.pressure,'Water');

e = in.relative_humidity*e_s/100;
N = 77.6/in.temperature * (in.pressure + 4810*e/in.temperature);
end

