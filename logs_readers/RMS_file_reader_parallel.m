function [out] = RMS_file_reader_parallel(filename)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%file format shown in
%https://www1.ncdc.noaa.gov/pub/data/igra/data/igra2-data-format.txt

if nargin == 0
    [file, path] = uigetfile('*.*');
    filename = fullfile(path,file);  
end
fid = fopen(filename);%create file variable and open file fname
k = 0;

while feof(fid) == 0% a~=-1
    current_string = fgetl(fid); %read new line
    if(contains(current_string,'#'))
        date_mass = strsplit(current_string,' ');
        k = k + 1;
        out(k).date.year = str2double(date_mass{2});
        out(k).date.month = str2double(date_mass{3});
        out(k).date.day = str2double(date_mass{4});
        out(k).date.hour = str2double(date_mass{5});
        if(str2double(date_mass{6}) ~= 9999)
          hour_and_minute = date_mass{6};
          out(k).date.hour  = str2double(hour_and_minute(1:2));
          out(k).date.minute  = str2double(hour_and_minute(3:4));
        else
          out(k).date.hour  = NaN;
          out(k).date.minute  = NaN;  
        end
        out(k).data_count = str2double(date_mass{7});
        out(k).data_sourse = date_mass{8};
        out(k).latitude = str2double(date_mass{9});
        out(k).longitude = str2double(date_mass{10});
        for i=1:out(k).data_count
            current_string = fgetl(fid); 
            out(k).raw_data(i,:) = current_string;
        end
    end
end
fclose(fid);
[out] = RMS_file_parser(out);
end