function [out] = RMS_file_parser(out)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
parfor k = 1:length(out)
    for i=1:out(k).data_count

        out(k).data(i,:).LVLTYP1 = str2double(out(k).raw_data(i,1));
        out(k).data(i,:).LVLTYP2 = str2double(out(k).raw_data(i,2));
        %%
        minute_and_second = out(k).raw_data(i,4:8);
        if(str2double(minute_and_second) ~= -9999 && str2double(minute_and_second) ~= -8888)
            out(k).data(i,:).minute  = str2double(minute_and_second(1:3));
            out(k).data(i,:).second = str2double(minute_and_second(4:5));
        else
            out(k).data(i,:).minute  = NaN;
            out(k).data(i,:).second  = NaN;
        end
        %% pressure
        pressure = str2double(out(k).raw_data(i,10:15));
        if(pressure ~= -9999)
            out(k).data(i,:).pressure = pressure/100; %in hPa
        else
            out(k).data(i,:).pressure  = NaN;
        end
        out(k).data(i,:).pressure_flag = out(k).raw_data(i,16);
        %% geopotential_height
        geopotential_height = str2double(out(k).raw_data(i,17:21));
        if(geopotential_height ~= -9999 && geopotential_height ~= -8888)
            out(k).data(i,:).geopotential_height = geopotential_height;% in meters
        else
            out(k).data(i,:).geopotential_height  = NaN;
        end
        out(k).data(i,:).geopotential_height_flag = out(k).raw_data(i,22);
        %% temperature
        temperature = str2double(out(k).raw_data(i,23:27));
        if(temperature ~= -9999 && temperature ~= -8888)
            out(k).data(i,:).temperature = temperature/10;% in C
        else
            out(k).data(i,:).temperature  = NaN;
        end
        out(k).data(i,:).temperature_flag = out(k).raw_data(i,28);
        %% relative_humidity
        relative_humidity = str2double(out(k).raw_data(i,29:33));
        if(relative_humidity ~= -9999 && relative_humidity ~= -8888)
            out(k).data(i,:).relative_humidity = relative_humidity/10; % in persent
        else
            out(k).data(i,:).relative_humidity  = NaN;
        end
        %% dewpoint_depression
        dewpoint_depression = str2double(out(k).raw_data(i,35:39));
        if(dewpoint_depression ~= -9999 && dewpoint_depression ~= -8888)
            out(k).data(i,:).dewpoint_depression = dewpoint_depression/10;% in C
        else
            out(k).data(i,:).dewpoint_depression  = NaN;
        end
        %% wind_direction
        wind_direction = str2double(out(k).raw_data(i,41:45));
        if(wind_direction ~= -9999 && wind_direction ~= -8888)
            out(k).data(i,:).wind_direction = wind_direction; %in degrees
        else
            out(k).data(i,:).wind_direction  = NaN;
        end
        %% wind_speed
        wind_speed = str2double(out(k).raw_data(i,47:51));
        if(wind_speed ~= -9999 && wind_speed ~= -8888)
            out(k).data(i,:).wind_speed = wind_speed/10; % in meters per second
        else
            out(k).data(i,:).wind_speed  = NaN;
        end
    end
end
out = rmfield(out,'raw_data');
end

