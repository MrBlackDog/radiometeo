function [out] = RMS_file_reader()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%file format shown in
%https://www1.ncdc.noaa.gov/pub/data/igra/data/igra2-data-format.txt

if nargin == 0
    [file, path] = uigetfile('*.*');
    filename = fullfile(path,file);  
end
fid = fopen(filename);%create file variable and open file fname
k = 0;

while feof(fid) == 0% a~=-1
    current_string = fgetl(fid); %read new line
    if(contains(current_string,'#'))
        date_mass = strsplit(current_string,' ');
        k = k + 1;
        out(k).date.year = str2double(date_mass{2});
        out(k).date.month = str2double(date_mass{3});
        out(k).date.day = str2double(date_mass{4});
        out(k).date.hour = str2double(date_mass{5});
        if(str2double(date_mass{6}) ~= 9999)
          hour_and_minute = date_mass{6};
          out(k).date.hour  = str2double(hour_and_minute(1:2));
          out(k).date.minute  = str2double(hour_and_minute(3:4));
        else
          out(k).date.hour  = NaN;
          out(k).date.minute  = NaN;  
        end
        out(k).data_count = str2double(date_mass{7});
        out(k).data_sourse = date_mass{8};
        out(k).latitude = str2double(date_mass{9});
        out(k).longitude = str2double(date_mass{10});
        for i=1:out(k).data_count
            current_string = fgetl(fid); 
%             out(k).raw_data(i,:) = current_string;
            out(k).data(i,:).LVLTYP1 = str2double(current_string(1));
            out(k).data(i,:).LVLTYP2 = str2double(current_string(2));
            %%
            minute_and_second = current_string(4:8);
            if(str2double(minute_and_second) ~= -9999 && str2double(minute_and_second) ~= -8888)             
                out(k).data(i,:).minute  = str2double(minute_and_second(1:3));
                out(k).data(i,:).second = str2double(minute_and_second(4:5));
            else
                out(k).data(i,:).minute  = NaN;
                out(k).data(i,:).second  = NaN;
            end
            %% pressure
            pressure = str2double(current_string(10:15));
            if(pressure ~= -9999)
               out(k).data(i,:).pressure = pressure/100; %in hPa
            else
               out(k).data(i,:).pressure  = NaN;
            end
            out(k).data(i,:).pressure_flag = current_string(16);
            %% geopotential_height
            geopotential_height = str2double(current_string(17:21));
            if(geopotential_height ~= -9999 && geopotential_height ~= -8888)
                out(k).data(i,:).geopotential_height = geopotential_height;% in meters
            else
                out(k).data(i,:).geopotential_height  = NaN;
            end
            out(k).data(i,:).geopotential_height_flag = current_string(22);
            %% temperature
            temperature = str2double(current_string(23:27));
            if(temperature ~= -9999 && temperature ~= -8888)
                out(k).data(i,:).temperature = temperature/10;% in C
            else
                out(k).data(i,:).temperature  = NaN;
            end
            out(k).data(i,:).temperature_flag = current_string(28);
            %% relative_humidity
            relative_humidity = str2double(current_string(29:33));
            if(relative_humidity ~= -9999 && relative_humidity ~= -8888)
               out(k).data(i,:).relative_humidity = relative_humidity/10; % in persent
            else
                out(k).data(i,:).relative_humidity  = NaN;
            end
            %% dewpoint_depression
            dewpoint_depression = str2double(current_string(35:39));
            if(dewpoint_depression ~= -9999 && dewpoint_depression ~= -8888)
                out(k).data(i,:).dewpoint_depression = dewpoint_depression/10;% in C
            else
                out(k).data(i,:).dewpoint_depression  = NaN;
            end
            %% wind_direction
            wind_direction = str2double(current_string(41:45));
            if(wind_direction ~= -9999 && wind_direction ~= -8888)
                out(k).data(i,:).wind_direction = wind_direction; %in degrees
            else
                out(k).data(i,:).wind_direction  = NaN;
            end
            %% wind_speed
            wind_speed = str2double(current_string(47:51));
            if(wind_speed ~= -9999 && wind_speed ~= -8888)
               out(k).data(i,:).wind_speed = wind_speed/10; % in meters per second
            else
                out(k).data(i,:).wind_speed  = NaN;
            end         
        end
    end
end
fclose(fid);
end

