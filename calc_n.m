function [n] = calc_n(in)

[N] =  calculate_N(in);
n = 1e-6*N + 1;

end

